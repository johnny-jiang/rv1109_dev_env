FROM ubuntu:18.04
COPY sources.list /etc/apt/
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y vim python3 python3-pip curl wget net-tools unzip tree git autoconf automake libtool cmake

RUN apt install -y language-pack-zh-hans && locale-gen zh_CN.UTF-8
ENV LC_ALL zh_CN.UTF-8
#RUN apt -q update && apt -qy install locales &&\
#  echo "LC_ALL=en_US.UTF-8" >> /etc/environment &&\
#  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen &&\
#  echo "LANG=en_US.UTF-8" > /etc/locale.conf &&\
#  locale-gen en_US.UTF-8

ENV TZ Asia/Shanghai
RUN apt -q update && apt -qy install tzdata &&\
  ln -snf /usr/share/zoneinfo/$TZ /etc/localtime &&\
  echo $TZ > /etc/timezone

RUN echo "alias sudo=''">> /root/.bash_aliases

EXPOSE 22
RUN apt -q update && apt -qy install openssh-client openssh-server &&\
  mkdir -p /var/run/sshd &&\
  sed -ri 's/StrictModes\s*yes/StrictModes no/g' /etc/ssh/sshd_config &&\
  sed -ri 's/UsePAM\s*yes/UsePAM no/g' /etc/ssh/sshd_config &&\
  sed -ri 's/session\s*required\s*pam_loginuid.so/#session required pam_loginuid.so/g' /etc/pam.d/sshd

RUN mkdir /docker_share
RUN ln -sf /usr/bin/python3 /usr/bin/python

# RV1109 dev enviorement
RUN apt install -y gcc-arm-linux-gnueabihf u-boot-tools device-tree-compiler gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev  autotools-dev libsigsegv2 m4 intltool libdrm-dev sed make binutils build-essential gcc g++ patch gzip gawk bzip2 perl tar cpio unzip rsync file bc libncurses5 libglib2.0-dev libgtk2.0-dev libglade2-dev mercurial rsync asciidoc w3m dblatex graphviz  libssl-dev expect fakeroot
RUN apt install -y python-linaro-image-tools linaro-image-tools libqt4-dev python-matplotlib lib32ncurses5 lib32z1
RUN apt install -y lib32gcc-7-dev g++-7 libstdc++-7-dev
COPY rv1126_rv1109_compiler_20210308.tar.gz /
RUN tar -zxf /rv1126_rv1109_compiler_20210308.tar.gz -C /
ENV ARCH arm
ENV CROSS_COMPILE arm-linux-gnueabihf-
ENV PATH /opt/rv1126_rv1109_sdk/buildroot/output/rockchip_face_board/host/bin:$PATH
