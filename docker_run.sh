#!/usr/bin/env bash

ssh_port=22345

docker ps -a | grep aiot_env_docker -o
if [ $? -eq 1 ];then
	docker run -dt --name aiot_env_docker -p $ssh_port:22 -v $HOME/:/docker_share/ aiot_env:v1 bash
	docker exec aiot_env_docker bash -c "service ssh start"
	docker exec -it aiot_env_docker bash
else
	docker ps -f status=running | grep aiot_env_docker -o
	if [ $? -eq 1 ];then
		docker start aiot_env_docker
		docker exec aiot_env_docker bash -c "service ssh start"
		docker exec -it aiot_env_docker bash
	else
		docker exec -it aiot_env_docker bash
	fi
fi

